#include <string>
#include <fstream>

static std::string LOG_DIR = ".";

inline std::string getCurrentDateTime(std::string s)
{
    time_t now = time(0);

    struct tm tstruct;
    char buf[80];
    tstruct = *localtime(&now);
    if (s == "now")
        strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct);
    else if (s == "date")
        strftime(buf, sizeof(buf), "%Y-%m-%d", &tstruct);
    return std::string(buf);
};
inline void Logger(std::string logMsg, LogLevel level, int code)
{
    std::ofstream logFile;
    logFile.open(LOG_DIR + getCurrentDateTime("date") + ".log", std::ios::app);
    logFile << getCurrentDateTime("now") << " " << getLogLevelString(level) << " " << code << " " << getStatusString(code) <<  " description: " << logMsg << std::endl;
    logFile.close();
}