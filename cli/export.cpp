#include "parser.h"
#include <string>
#include <vector>
#include "../src/Image.h"
#include "../src/ImageProc.h"
#include "../src/Channel.h"
#include <algorithm>
#include "Debug.h"
#include "json.hpp"

static int DEBUG_STATE = 0;
static int JSON_OUTPUT = 0;

using namespace nlohmann;

/* ----------------------- Global variables -----------------------
Static variables for the runtime console output and log output
*/

using namespace Image;

int main(int argc, char *argv[])
{
    JsonLogger json_logger;
    Parser parser;
    if (parser.parse(argc, argv) == false)
    {

        json_logger["Status"] = ERROR_CODE::ARG_ERROR;
        json_logger.show();
        DEBUG("Argument parsing failed", LOG_ERROR, ERROR_CODE::ARG_ERROR, DEBUG::CONSOLE_LOG);
        return -1;
    }
    if (parser.console_type->type == ConsoleType::NONE)
    {
        DEBUG_STATE = DEBUG::NO_LOG;
        JSON_OUTPUT = 0;
    }
    else if (parser.console_type->type == ConsoleType::JSON)
    {
        DEBUG_STATE = DEBUG::NO_LOG;
        JSON_OUTPUT = 1;
    }
    else if (parser.console_type->type == ConsoleType::TEXT)
    {

        DEBUG_STATE = DEBUG::CONSOLE_LOG;
        JSON_OUTPUT = 0;
    }

    if (parser.isLogValid())
    {
        LOG_PATH = parser.log_dir();
        if (DEBUG_STATE == DEBUG::CONSOLE_LOG)
        {
            DEBUG_STATE = DEBUG::BOTH;
        }
        else
        {
            DEBUG_STATE = DEBUG::FILE_LOG;
        }
    }

    std::vector<std::string> input_files = parser.get_input_file_paths();
    std::vector<std::string> output_file = parser.get_output_file_paths();

    if (input_files.empty())
    {

        DEBUG("No input files specified", LOG_ERROR, ERROR_CODE::ARG_ERROR, DEBUG_STATE);
        if (JSON_OUTPUT == 1)
        {
            json_logger["Status"] = ERROR_CODE::ARG_ERROR;
            json_logger.show();
        }

        return -1;
    }

    // start processing the images
    for (int i = 0; i < input_files.size(); i++)
    {
        json_logger.clear();

        DEBUG("Opening file: " + input_files[i], LOG_INFO, INFO, DEBUG_STATE);

        Image::Image *image = new Image::Image();
        // open the image

        int retval = image->open(input_files[i].c_str());
        if (retval < 0)
        {
            DEBUG("file couldn't opened \t" + input_files[i], LOG_ERROR, retval, DEBUG_STATE);
            if (JSON_OUTPUT == 1)
            {
                json_logger["Status"] = ERROR_CODE::INTERNAL_ERROR;
                json_logger.show();
            }
            continue;
        }
        else
            DEBUG("file opened successfully \t" + input_files[i], LOG_INFO, INFO, DEBUG_STATE);

        if (parser.isInfoValid())
        {
            JsonLogger Channels;

            for (int i = 0; i < image->channelCount; i++)
            {
                std::string ch = "Channel_" + std::to_string(i);
                Channels["Channels"][ch] = {
                    {"Color_space", image->channels[i]->colorspace}};

                JsonLogger color;
                color.addbuffer("Color", image->channels[i]->color, 4);
                Channels["Channels"][ch]["Color"] = color["Color"];
            };

            JsonLogger exifdata;
            exifdata.addbuffer("ExifRawData", image->exifdata, image->sizeOfExifData);

            JsonLogger xmpdata;
            xmpdata.addbuffer("XmpMetaRawData", image->xmpmetadata, image->sizeOfXmpMetaData);

            JsonLogger iccprofile;
            iccprofile.addbuffer("ICCProfileRawData", image->exifdata, image->sizeOfExifData);

            JsonLogger info;

            info["Info"]["width"] = image->width;
            info["Info"]["height"] = image->height;
            info["Info"]["channel_count"] = image->channelCount;
            info["Info"]["color_mode"] = image->colorMode;
            info["Info"]["bit_depth"] = image->depth;
            info["Info"]["Channels"] = Channels["Channels"];
            info["Info"]["ICCProfile"] = iccprofile["ICCProfileRawData"];
            info["Info"]["ExifData"] = exifdata["ExifRawData"];
            info["Info"]["XmpData"] = xmpdata["XmpMetaRawData"];
            iccprofile.clear();
            exifdata.clear();
            xmpdata.clear();
            json_logger["Info"] = info["Info"];
        }

        // if resize is valid then resize the image
        if (parser.isResizeValid())
        {
            // if both width and height are specified
            if (parser.width() != 0 && parser.height() != 0)
            {
                // if image is smaller than the desired output size
                //if (image->width < parser.width() || image->height < parser.height())
                //{
                //    image->Free();

                //    DEBUG("Image is smaller than the desired output size", LOG_ERROR, ERROR_CODE::ARG_ERROR, DEBUG_STATE);
                //    if (JSON_OUTPUT == 1)
                //    {
                //        json_logger["Status"] = ERROR_CODE::ARG_ERROR;
                //        json_logger.show();
                //    }
                //    continue;
                //}

                // create a new image for resized image
                Image::Image *resizedImage = new Image::Image();
                int retval = Image::ImageProc::resizeImage(image, resizedImage, parser.width(), parser.height(), true);
                if (retval < 0)
                {
                    image->Free();
                    resizedImage->Free();

                    DEBUG("Error: Couldn't resize the image", LOG_ERROR, retval, DEBUG_STATE);
                    if (JSON_OUTPUT == 1)
                    {
                        json_logger["Status"] = retval;
                        json_logger.show();
                    }

                    continue;
                }
                else
                    DEBUG("Image resized ", LOG_INFO, INFO, DEBUG_STATE);

                // save the resized image
                retval = resizedImage->save(output_file[i].c_str(), parser.export_type().c_str());
                if (retval < 0)
                {
                    image->Free();
                    resizedImage->Free();

                    DEBUG("Error: Couldn't save the file" + output_file[i], LOG_ERROR, retval, DEBUG_STATE);
                    DEBUG("Closing image" + input_files[i], LOG_INFO, INFO, DEBUG_STATE);
                    if (JSON_OUTPUT == 1)
                    {
                        json_logger["Status"] = retval;
                        json_logger.show();
                    }
                    continue;
                }
                else
                {
                    image->Free();
                    resizedImage->Free();

                    DEBUG("File saved successfully" + output_file[i], LOG_INFO, SUCCES, DEBUG_STATE);
                    DEBUG("Closing image" + input_files[i], LOG_INFO, INFO, DEBUG_STATE);

                    if (JSON_OUTPUT == 1)
                    {
                        json_logger["Status"] = retval;
                        json_logger.show();
                    }
                    continue;
                }
            }
            // if only given edge size
            else if (parser.edge_size() != 0)
            {
                // if image is smaller than the desired output size
                //if (image->width < parser.edge_size() && image->height < parser.edge_size())
                //{

                //    image->Free();

                //    DEBUG("Image is smaller than the desired output size", LOG_ERROR, ERROR_CODE::ARG_ERROR, DEBUG_STATE);
                //    DEBUG("Image Closed", LOG_INFO, INFO, DEBUG_STATE);
                //    if (JSON_OUTPUT == 1)
                //    {
                //        json_logger["Status"] = retval;
                //        json_logger.show();
                //    }

                //    continue;
                //}
                // set the resize width and height
                unsigned int resize_width, resize_height;
                if (image->width > image->height)
                {
                    resize_width = parser.edge_size();
                    resize_height = (image->height * parser.edge_size()) / image->width;
                }
                else
                {
                    resize_height = parser.edge_size();
                    resize_width = (image->width * parser.edge_size()) / image->height;
                }
                // create a new image for resized image
                Image::Image *resizedImage = new Image::Image();

                int retval = Image::ImageProc::resizeImage(image, resizedImage, resize_width, resize_height, true);
                if (retval < 0)
                {
                    image->Free();
                    resizedImage->Free();

                    DEBUG("Error: Couldn't resize the image", LOG_ERROR, retval, DEBUG_STATE);
                    DEBUG("Image Closed", LOG_INFO, INFO, DEBUG_STATE);
                    if (JSON_OUTPUT == 1)
                    {
                        json_logger["Status"] = retval;
                        json_logger.show();
                    }

                    continue;
                }
                else
                {
                    DEBUG("Image resized ", LOG_INFO, INFO, DEBUG_STATE);
                }

                // save the resized image
                retval = resizedImage->save(output_file[i].c_str(), parser.export_type().c_str());
                if (retval < 0)
                {
                    image->Free();
                    resizedImage->Free();

                    DEBUG("Error: Couldn't save the file" + output_file[i], LOG_ERROR, retval, DEBUG_STATE);
                    DEBUG("Closing image" + input_files[i], LOG_INFO, INFO, DEBUG_STATE);
                    if (JSON_OUTPUT == 1)
                    {
                        json_logger["Status"] = retval;
                        json_logger.show();
                    }

                    continue;
                }
                else
                {
                    image->Free();
                    resizedImage->Free();

                    DEBUG("File saved successfully" + output_file[i], LOG_INFO, SUCCES, DEBUG_STATE);
                    DEBUG("Closing image" + input_files[i], LOG_INFO, INFO, DEBUG_STATE);

                    if (JSON_OUTPUT == 1)
                    {
                        json_logger["Status"] = retval;
                        json_logger.show();
                    }

                    continue;
                }
            }
        }
        else
        {
            // save the image
            int retval = image->save(output_file[i].c_str(), parser.export_type().c_str());

            if (retval < 0)
            {
                image->Free();

                DEBUG("Error: Couldn't save the file" + output_file[i], LOG_ERROR, retval, DEBUG_STATE);
                DEBUG("Closing image" + input_files[i], LOG_INFO, INFO, DEBUG_STATE);
                if (JSON_OUTPUT == 1)
                {
                    json_logger["Status"] = retval;
                    json_logger.show();
                }

                continue;
            }
            else
            {
                image->Free();

                DEBUG("File saved successfully" + output_file[i], LOG_INFO, SUCCES, DEBUG_STATE);
                DEBUG("Closing image" + input_files[i], LOG_INFO, INFO, DEBUG_STATE);

                if (JSON_OUTPUT == 1)
                {
                    json_logger["Status"] = retval;
                    json_logger.show();
                }

                continue;
            }
        }
    }
    return 0;
}
