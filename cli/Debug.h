#include <string>
#include <fstream>
#include "json.hpp"
#include "../src/ErrorCode.h"

static std::string LOG_PATH = ".";

enum DEBUG
{
    FILE_LOG = 1,
    CONSOLE_LOG = 2,
    BOTH = 3,
    NO_LOG = 4
};

class JsonLogger : public nlohmann::json
{
public:
    JsonLogger()
    {
        (*this)["Status"] = this->status;
    };

    int status;
    template <typename T>
    void addbuffer(std::string key, T *buffer, size_t size)
    {
        (*this)[key] = nlohmann::json::array();
        for (size_t i = 0; i < size; i++)
        {
            (*this)[key].push_back(buffer[i]);
        }
    };
    void addjson(std::string key, nlohmann::json json)
    {
        (*this)[key] = json;
    };
    void show()
    {
        std::cout << (*this).dump() << std::endl;
    };

    void save(std::string path)
    {
        std::ofstream logFile;
        logFile.open(path, std::ios::app);
        logFile << (*this).dump() << std::endl;
        logFile.close();
    };
};

/// \ingroup cli
/// \brief LogLevel on cli
enum LogLevel
{
    LOG_DEBUG = 10,
    LOG_INFO = 20,
    LOG_WARN = 30,
    LOG_ERROR = 40,
    LOG_FATAL = 50,
};

/// \ingroup cli
/// \brief ERROR_CODE on cli
enum ERROR_CODE
{
    ARG_ERROR = 400,
    FILE_NOT_FOUND = 404,
    INTERNAL_ERROR = 408
};

/// \ingroup cli
/// \brief Status code on cli
#define SUCCES 100
#define INFO 101

/// \ingroup cli
/// \brief Get the status string from the status code
/// \param val The status code
/// \return The status string
inline std::string getStatusString(int val)
{
    switch (val)
    {
    case INFO:
        return "Process Information";
    case SUCCES:
        return "Succes";
    case ERROR_CODE::ARG_ERROR:
        return "Invalid Arguments.";
    case ERROR_CODE::FILE_NOT_FOUND:
        return "File Not Found";
    case ERROR_CODE::INTERNAL_ERROR:
        return "Failed.";
    default:
        return "Unknown.";
    }
}

/// \ingroup cli
/// \brief Get the log level string from the log level
/// \param level The log level
std::string getLogLevelString(int level)
{
    switch (level)
    {
    case LOG_DEBUG:
        return "DEBUG";
    case LOG_INFO:
        return "INFO";
    case LOG_WARN:
        return "WARN";
    case LOG_ERROR:
        return "ERROR";
    case LOG_FATAL:
        return "FATAL";
    default:
        return "UNKNOWN";
    }
};

/// @brief  console log
/// @param message decsription of the log
/// @param level  log level
/// @param code status or erroc code

/// @brief  get current date and time
/// @param s now or date
/// @return string of current date and time
std::string getCurrentDateTime(std::string s)
{
    time_t now = time(0);

    struct tm tstruct;
    char buf[80];
    tstruct = *localtime(&now);
    if (s == "now")
        strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct);
    else if (s == "date")
        strftime(buf, sizeof(buf), "%Y-%m-%d", &tstruct);
    return std::string(buf);
};

void ConsoleLog(std::string message, int level, int code)
{

    std::string code_string = DecodeErrorCode(code);
    if (code_string == "Unknown error.")
    {
        code_string = getStatusString(code);
    }
    std::cout << getLogLevelString(level) << ": " << code_string << ". description: " << message << std::endl;

    return;
}

/// @brief  json log
/// @param message decsription of the log
/// @param level  log level
/// @param enabled enable or disable the log

/// @brief  file log
/// @param message decsription of the log
/// @param level  log level
/// @param enabled enable or disable the log
/// @param code status or erroc code
void FileLog(std::string message, int level, int code)
{

    std::ofstream logFile;
    logFile.open(LOG_PATH, std::ios::app);

    std::string code_string = DecodeErrorCode(code);
    if (code_string == "Unknown error.")
    {
        code_string = getStatusString(code);
    }

    logFile << getCurrentDateTime("now") << " " << getLogLevelString(level) << " " << code << " " << code_string << " description: " << message << std::endl;
    logFile.close();
    return;
}

/// @brief  DEBUG
/// @param message decsription of the log
/// @param level  log level
/// @param code status or error code
/// @param DEBUG  log level
void DEBUG(std::string message, int level, int code, int DEBUG)
{
    switch (DEBUG)
    {
    case FILE_LOG:
        FileLog(message, level, code);
        break;
    case CONSOLE_LOG:
        ConsoleLog(message, level, code);
        break;
    case BOTH:
        FileLog(message, level, code);
        ConsoleLog(message, level, code);
        break;
    case NO_LOG:
        break;
    default:
        break;
    }
    return;
}
