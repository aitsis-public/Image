#install requirements
git submodule update --init --recursive

VERSION=$(grep -i "project(Image VERSION" CMakeLists.txt | cut -d '"' -f2)
mkdir build
PACKAGE_DIR=Image_${VERSION}_amd64
cmake -S . -B build -DCMAKE_INSTALL_PREFIX=${PACKAGE_DIR}/usr/local -DCMAKE_BUILD_TYPE=Release
make -C build && make -C build install
mkdir ${PACKAGE_DIR}/DEBIAN
cat << EOF > $PACKAGE_DIR/DEBIAN/control
Package: Image
Version: ${VERSION}
Architecture: amd64
Maintainer: Yasin Tunçer yasin@ait.com.tr
Description: Psd image exporter
 Extract psd images to jpeg or png file formats. exports image information. Resize exported image.
EOF

dpkg-deb --build $PACKAGE_DIR

PACKAGE=$(ls | grep *.deb)
  
VERSION=$(echo $PACKAGE | cut -d '_' -f2)

NAME=$(echo $PACKAGE | cut -d '_' -f1)

dpkg -i $PACKAGE

