# IMAGE

An Image project that aim's to support all large image file format. Currently only supports psd files
Image lib can decode psd files, has image process tools and export tools.

## Requirements

-   libjpeg-dev libpng-dev

## Build Requirement
-   cmake >= 3.17
-   make 
-   c++11 and up

## Setup

    chmod +x ./setup.sh; ./setup.sh



## Usage
Usage:
    
    psd_image_exporter  
    
    -h or --help:           Prints the help message

    -i or --info:           Prints the info of the image

    -f or --file:           Reads the image from the file path

    -d or --dir:            Reads the images from the directory

    --recursive:            Reads the images from the directory recursively

    -r or --resize:         Resizes the image to the given size <width>x<height> or <size of one edge>

    -e or _export_type:     Sets the export type of the image <png|jpg>

    --output_dir:           Sets the output directory of the image.
                         if not defined output directory created 'result' in the input directory

    --output_adjective:     Adds the addjective before the file name

    --output_name:          Sets the output name of the image.
                         if not defined output name get same as input name

    --log_dir <path>:       Sets the log directory

    -c or --console_type <json>|<text>:     Sets the console output. Default text