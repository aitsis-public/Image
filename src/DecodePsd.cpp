// Image library includes
#include "Image.h"
#include "DecodePsd.h"
#include "ErrorCode.h"
// Psd sdk includes
#include "Psd.h"
#include "PsdDocument.h"
#include "PsdStringUtil.h"
#include "PsdMallocAllocator.h"
#include "PsdNativeFile.h"
#include "PsdColorMode.h"
#include "PsdAlphaChannel.h"
#include "PsdPlanarImage.h"
#include "PsdImageResourcesSection.h"
#include "PsdImageDataSection.h"
#include "PsdParseDocument.h"
#include "PsdParseImageDataSection.h"
#include "PsdParseImageResourcesSection.h"
#include "Psdinttypes.h"

// C++ includes
#include <stdlib.h>
#include <cmath>
#include <cstring>
#include <iostream>
PSD_USING_NAMESPACE;
//-------------------- Helper functions --------------------
namespace
{
    // ---------------------------------------------------------------------------------------------
    // remapBuffer - remap buffer to 8-bit
    template <typename T>
    uint8_t *remapBuffer(T *buffer, unsigned int count)
    {
        double quantization_coef = 255.0 / std::pow(2, sizeof(T) * 8);
        uint8_t *new_buffer = (uint8_t *)malloc(count * sizeof(uint8_t));
        for (unsigned int i = 0; i < count; ++i)
        {
            new_buffer[i] = (uint8_t)(buffer[i] * quantization_coef);
        }
        return new_buffer;
    }

    // ---------------------------------------------------------------------------------------------
    // remap - remap value from one range to another
    template <typename T>
    double remap(T src, double src_max, double src_min, double dst_max, double dst_min)
    {
        return (src - src_min) / (src_max - src_min) * (dst_max - dst_min) + dst_min;
    }

    // ---------------------------------------------------------------------------------------------
    // CopyRemainingPixels - copy remaining pixels to destination buffer
    template <typename T>
    void CopyRemainingPixels(void *src, uint8_t *dst, unsigned int count)
    {
        if (sizeof(T) > sizeof(uint8_t))
        {
            const T *buffer = static_cast<const T *>(src);
            memcpy(dst, remapBuffer(buffer, count), count);
        }
        else
        {
            memcpy(dst, src, count);
        }
    }

    // ---------------------------------------------------------------------------------------------
    // CopyRemainingPixels - copy remaining pixels to destination buffer
    void GetColorTable(uint16_t *src, double *dst, uint16_t colorspace)
    {
        switch (colorspace)
        {
        case psd::COLOR_SPACE::RGB:
            dst[0] = remap(src[0], 65535.0, 0.0, 255.0, 0.0);
            dst[1] = remap(src[1], 65535.0, 0.0, 255.0, 0.0);
            dst[2] = remap(src[2], 65535.0, 0.0, 255.0, 0.0);
            dst[3] = remap(src[3], 65535.0, 0.0, 255.0, 0.0);
            break;
        case psd::COLOR_SPACE::CMYK:
            dst[0] = 100 - remap(src[0], 65535.0, 0.0, 100.0, 0.0);
            dst[1] = 100 - remap(src[1], 65535.0, 0.0, 100.0, 0.0);
            dst[2] = 100 - remap(src[2], 65535.0, 0.0, 100.0, 0.0);
            dst[3] = 100 - remap(src[3], 65535.0, 0.0, 100.0, 0.0);
            break;

        case psd::COLOR_SPACE::LAB:
            dst[0] = remap(src[0], 65535.0, 0.0, 100.0, 0.0);
            dst[1] = remap(src[1], 65535.0, 0.0, 100.0, 0.0);
            dst[2] = remap(src[2], 65535.0, 0.0, 255.0, 0.0);
            dst[3] = remap(src[3], 65535.0, 0.0, 255.0, 0.0);
            break;

        case psd::COLOR_SPACE::HSB:
            dst[0] = remap(src[0], 65535.0, 0.0, 360.0, 0.0);
            dst[1] = remap(src[1], 65535.0, 0.0, 100.0, 0.0);
            dst[2] = remap(src[2], 65535.0, 0.0, 100.0, 0.0);
            dst[3] = remap(src[3], 65535.0, 0.0, 255.0, 0.0);
            break;
        }
    }
}
IMAGE_NAMESPACE_BEGIN
namespace DecodePsd
{

    int getChannelData(void *data, Channel *channel)
    {
        switch (channel->bitsPerPixel)
        {
        case 8:
            CopyRemainingPixels<uint8_t>(data, channel->data, channel->size);
            break;
        case 16:
            CopyRemainingPixels<uint16_t>(data, channel->data, channel->size);
            break;
        case 32:
            CopyRemainingPixels<float32_t>(data, channel->data, channel->size);
            break;
        default:
            delete[] channel->data;
            channel->data = nullptr;
            return UNKNOWN_BIT_DEPTH;
        }
        return 0;
    }

    // ---------------------------------------------------------------------------------------------
    // getChannel - get channel data from psd file
    int getAlphaChannel(void *data, psd::Document *doc, Channel *channel, psd::AlphaChannel* channel_resource, int channelIndex)
    {
        int channel_size = doc->width * doc->height * sizeof(uint8_t);
        channel->size = channel_size;
        channel->bitsPerPixel = doc->bitsPerChannel;
        channel->data = new uint8_t[channel_size];
        channel->colorspace = channel_resource->colorSpace;
        int result = getChannelData(data, channel);
        GetColorTable(channel_resource->color, channel->color, channel_resource->colorSpace);
        return result;
    }

    int getRGBChannel(void *data, psd::Document *doc, Channel *channel, int channelIndex)
    {
        int channel_size = doc->width * doc->height * sizeof(uint8_t);
        channel->size = channel_size;
        channel->bitsPerPixel = doc->bitsPerChannel;
        channel->colorspace = psd::COLOR_SPACE::RGB;
        switch (channelIndex)
        {
        case 0:
            channel->color[0] = 255;
            channel->color[1] = 0;
            channel->color[2] = 0;
            channel->color[3] = 0;
            break;
        case 1:
            channel->color[0] = 0;
            channel->color[1] = 255;
            channel->color[2] = 0;
            channel->color[3] = 0;
            break;
        case 2:
            channel->color[0] = 0;
            channel->color[1] = 0;
            channel->color[2] = 255;
            channel->color[3] = 0;
            break;
        case 3:
            channel->color[0] = 0;
            channel->color[1] = 0;
            channel->color[2] = 0;
            channel->color[3] = 255;
            break;
        default:
            channel->color[0] = 0;
            channel->color[1] = 0;
            channel->color[2] = 0;
            channel->color[3] = 0;
            break;
        }
        channel->data = new uint8_t[channel_size];
        return getChannelData(data, channel);
        return 0;
    }

    int getCMYKChannel(void *data, psd::Document *doc, Channel *channel, int channelIndex)
    {
        int channel_size = doc->width * doc->height * sizeof(uint8_t);
        channel->size = channel_size;
        channel->bitsPerPixel = doc->bitsPerChannel;
        channel->data = new uint8_t[channel_size];
        channel->colorspace = psd::COLOR_SPACE::CMYK;
        channel->data = new uint8_t[channel_size];
        int result = getChannelData(data, channel);
        switch (channelIndex)
        {
        case 0:
        {
            channel->color[0] = 100;
            channel->color[1] = 0;
            channel->color[2] = 0;
            channel->color[3] = 0;
        }
        break;
        case 1:
        {
            channel->color[0] = 0;
            channel->color[1] = 100;
            channel->color[2] = 0;
            channel->color[3] = 0;
        }
        break;
        case 2:
        {
            channel->color[0] = 0;
            channel->color[1] = 0;
            channel->color[2] = 100;
            channel->color[3] = 0;
        }
        break;
        case 3:
        {
            channel->color[0] = 0;
            channel->color[1] = 0;
            channel->color[2] = 0;
            channel->color[3] = 100;
        }
        break;
        default:
        {
            channel->color[0] = 0;
            channel->color[1] = 0;
            channel->color[2] = 0;
            channel->color[3] = 0;
        }
        break;
        }

        return result;
    }

    int getLABChannel(void *data, psd::Document *doc, Channel *channel, int channelIndex)
    {
        int channel_size = doc->width * doc->height * sizeof(uint8_t);
        channel->size = channel_size;
        channel->bitsPerPixel = doc->bitsPerChannel;
        channel->data = new uint8_t[channel_size];
        channel->colorspace = psd::COLOR_SPACE::CMYK;
        channel->data = new uint8_t[channel_size];
        int result = getChannelData(data, channel);
        switch (channelIndex)
        {
        case 0:
        {
            channel->color[0] = 100;
            channel->color[1] = 0;
            channel->color[2] = 0;
            channel->color[3] = 0;
        }
        break;
        case 1:
        {
            channel->color[0] = 0;
            channel->color[1] = 100;
            channel->color[2] = 0;
            channel->color[3] = 0;
        }
        break;
        case 2:
        {
            channel->color[0] = 0;
            channel->color[1] = 0;
            channel->color[2] = 100;
            channel->color[3] = 0;
        }
        break;
        case 3:
        {
            channel->color[0] = 0;
            channel->color[1] = 0;
            channel->color[2] = 0;
            channel->color[3] = 100;
        }
        break;
        default:
        {
            channel->color[0] = 0;
            channel->color[1] = 0;
            channel->color[2] = 0;
            channel->color[3] = 0;
        }
        break;
        }

        return result;
    }

    int getGrayscaleChannel(void *data, psd::Document *doc, Channel *channel, int channelIndex)
    {
        int channel_size = doc->width * doc->height * sizeof(uint8_t);
        channel->size = channel_size;
        channel->bitsPerPixel = doc->bitsPerChannel;
        channel->data = new uint8_t[channel_size];
        channel->colorspace = psd::COLOR_SPACE::GRAY;
        channel->data = new uint8_t[channel_size];
        int result = getChannelData(data, channel);

        channel->color[0] = 1;
        channel->color[1] = 1;
        channel->color[2] = 1;
        channel->color[3] = 1;
        return result;
    }

    int getBitmapChannel(void *data, psd::Document *doc, Channel *channel, int channelIndex)
    {
        int channel_size = doc->width * doc->height * sizeof(uint8_t);
        channel->size = channel_size;
        channel->bitsPerPixel = doc->bitsPerChannel;
        channel->data = new uint8_t[channel_size];
        channel->colorspace = psd::COLOR_SPACE::GRAY;
        int result = getChannelData(data, channel);
        channel->color[0] = 1;
        channel->color[1] = 1;
        channel->color[2] = 1;
        channel->color[3] = 1;
        return result;
    }

    int getImageResource(psd::ImageResourcesSection *resource, Image *image)
    {
        if (resource->iccProfile != nullptr)
        {
            image->iccprofile = (uint8_t *)malloc(resource->sizeOfICCProfile);
            image->sizeOfIccProfile = resource->sizeOfICCProfile;
            memcpy(image->iccprofile, resource->iccProfile, resource->sizeOfICCProfile);
        }
        if (resource->exifData != nullptr)
        {
            image->exifdata = (uint8_t *)malloc(resource->sizeOfExifData);
            image->sizeOfExifData = resource->sizeOfExifData;
            memcpy(image->exifdata, resource->exifData, resource->sizeOfExifData);
        }
        if (resource->xmpMetadata != nullptr)
        {
            image->xmpmetadata = (uint8_t *)malloc(resource->sizeOfXmpMetadata);
            image->sizeOfXmpMetaData = resource->sizeOfXmpMetadata;
            memcpy(image->xmpmetadata, resource->xmpMetadata, resource->sizeOfXmpMetadata);
        }
        return 0;
    }

    // ---------------------------------------------------------------------------------------------
    // DecodePSD - decode psd file
    int open(const char *filename, Image *image)
    {
        // Create allocator and file object
        psd::MallocAllocator allocator;
        psd::NativeFile file(&allocator);

        // Open file
        const wchar_t *wpath = stringUtil::ConvertString(filename);

        if (!file.OpenRead(wpath))
        {
            return FAILED_OPEN_FILE;
        }

        // Create document
        psd::Document *document;
        document = psd::CreateDocument(&file, &allocator);
        if (!document)
        {
            file.Close();
            return FAILED_TO_PARSE_IMAGE;
        }

        image->depth = document->bitsPerChannel;
        // indexed, duotone, bitmap color modes are not supported yet
        if (document->colorMode == psd::colorMode::INDEXED || document->colorMode == psd::colorMode::DUOTONE)
        {
            psd::DestroyDocument(document, &allocator);
            file.Close();
            return FAILED_TO_PARSE_IMAGE;
        }

        // check if image data section available. the image data section stores the final, merged image data,
        //      as well as alpha channel data this only available when saving the document with "Maximize Compatibility" turned on.
        if (document->imageDataSection.length == 0)
        {
            psd::DestroyDocument(document, &allocator);
            file.Close();
            return FAILED_TO_PARSE_IMAGE;
        }

        // Parse image resources section and image data section
        psd::ImageResourcesSection *resourcesSection = psd::ParseImageResourcesSection(document, &file, &allocator);
        if (resourcesSection)
        { // get image resources section data to image struct
            getImageResource(resourcesSection, image);
        }
        else
        {
            psd::DestroyDocument(document, &allocator);
            file.Close();
            return FAILED_TO_PARSE_IMAGE;
        }

        psd::ImageDataSection *dataSection = psd::ParseImageDataSection(document, &file, &allocator);

        if (!dataSection)
        {
            psd::DestroyDocument(document, &allocator);
            psd::DestroyImageResourcesSection(resourcesSection, &allocator);
            psd::DestroyImageDataSection(dataSection, &allocator);
            file.Close();
            return FAILED_TO_PARSE_IMAGE;
        }

        unsigned int channel_count = document->channelCount;
        image->channels = new Channel *[channel_count];

        // get channels
        switch (document->colorMode)
        {
        case psd::colorMode::RGB:
        {
            for (unsigned int i = 0; i < 3; i++)
            {
                image->channels[i] = new Channel();
                int result = getRGBChannel(dataSection->images[i].data, document, image->channels[i], i);
                // if failed to get channel, destroy document  resources and data section and return
                if (result < 0)
                {
                    psd::DestroyDocument(document, &allocator);
                    psd::DestroyImageResourcesSection(resourcesSection, &allocator);
                    psd::DestroyImageDataSection(dataSection, &allocator);
                    file.Close();
                    return result;
                }
            }
            if (channel_count > 3)
            {
                psd::AlphaChannel *channel_resource = &resourcesSection->alphaChannels[0];
                for (unsigned int i = 0; i < resourcesSection->alphaChannelCount; i++)
                {
                    image->channels[i+3] = new Channel();
                    int result = getAlphaChannel(dataSection->images[i+3].data, document, image->channels[i+3], channel_resource, i);
                    // if failed to get channel, destroy document  resources and data section and return
                    if (result < 0)
                    {
                        psd::DestroyDocument(document, &allocator);
                        psd::DestroyImageResourcesSection(resourcesSection, &allocator);
                        psd::DestroyImageDataSection(dataSection, &allocator);
                        file.Close();
                        return result;
                    }
                    channel_resource++;
                }
            }
        }
        break;
        case psd::colorMode::CMYK:
        {
            for (unsigned int i = 0; i < 4; i++)
            {
                image->channels[i] = new Channel();
                int result = getCMYKChannel(dataSection->images[i].data, document, image->channels[i], i);
                // if failed to get channel, destroy document  resources and data section and return
                if (result < 0)
                {
                    psd::DestroyDocument(document, &allocator);
                    psd::DestroyImageResourcesSection(resourcesSection, &allocator);
                    psd::DestroyImageDataSection(dataSection, &allocator);
                    file.Close();
                    return result;
                }
            }
            if (channel_count > 4)
            {
                psd::AlphaChannel *channel_resource = &resourcesSection->alphaChannels[0];

                for (unsigned int i = 0; i < resourcesSection->alphaChannelCount; i++)
                {
                    image->channels[i+4] = new Channel();
                    int result = getAlphaChannel(dataSection->images[i+4].data, document, image->channels[i+4], channel_resource, i);
                    // if failed to get channel, destroy document  resources and data section and return
                    if (result < 0)
                    {
                        psd::DestroyDocument(document, &allocator);
                        psd::DestroyImageResourcesSection(resourcesSection, &allocator);
                        psd::DestroyImageDataSection(dataSection, &allocator);
                        file.Close();
                        return result;
                    }
                    channel_resource++;
                }
            }
        }
        break;
        case psd::colorMode::LAB:
        {
            for (unsigned int i = 0; i < 3; i++)
            {
                image->channels[i] = new Channel();
                int result = getLABChannel(dataSection->images[i].data, document, image->channels[i], i);
                // if failed to get channel, destroy document  resources and data section and return
                if (result < 0)
                {
                    psd::DestroyDocument(document, &allocator);
                    psd::DestroyImageResourcesSection(resourcesSection, &allocator);
                    psd::DestroyImageDataSection(dataSection, &allocator);
                    file.Close();
                    return result;
                }
            }
            if (channel_count > 3)
            {
                psd::AlphaChannel *channel_resource = &resourcesSection->alphaChannels[0];
                for (unsigned int i = 0; i < resourcesSection->alphaChannelCount; i++)
                {
                    image->channels[i+3] = new Channel();
                    int result = getAlphaChannel(dataSection->images[i+3].data, document, image->channels[i+3], channel_resource, i);
                    // if failed to get channel, destroy document  resources and data section and return
                    if (result < 0)
                    {
                        psd::DestroyDocument(document, &allocator);
                        psd::DestroyImageResourcesSection(resourcesSection, &allocator);
                        psd::DestroyImageDataSection(dataSection, &allocator);
                        file.Close();
                        return result;
                    }
                    channel_resource++;
                }
            }
        }
        break;

        case psd::colorMode::GRAYSCALE:
        {
            image->channels[0] = new Channel();
            int result = getGrayscaleChannel(dataSection->images[0].data, document, image->channels[0], 0);
            // if failed to get channel, destroy document  resources and data section and return
            if (result < 0)
            {
                psd::DestroyDocument(document, &allocator);
                psd::DestroyImageResourcesSection(resourcesSection, &allocator);
                psd::DestroyImageDataSection(dataSection, &allocator);
                file.Close();
                return result;
            }
            if (channel_count > 1)
            {
                psd::AlphaChannel *channel_resource = &resourcesSection->alphaChannels[0];
                for (unsigned int i =0; i < channel_count; i++)
                {
                    image->channels[i+1] = new Channel();
                    int result = getAlphaChannel(dataSection->images[i+1].data, document, image->channels[i+1], channel_resource, i);
                    // if failed to get channel, destroy document  resources and data section and return
                    if (result < 0)
                    {
                        psd::DestroyDocument(document, &allocator);
                        psd::DestroyImageResourcesSection(resourcesSection, &allocator);
                        psd::DestroyImageDataSection(dataSection, &allocator);
                        file.Close();
                        return result;
                    }
                    channel_resource++;
                }
            }
        }

        break;
        case psd::colorMode::BITMAP:
        {
            image->channels[0] = new Channel();
            int result = getBitmapChannel(dataSection->images[0].data, document, image->channels[0], 0);
            // if failed to get channel, destroy document  resources and data section and return
            if (result < 0)
            {
                psd::DestroyDocument(document, &allocator);
                psd::DestroyImageResourcesSection(resourcesSection, &allocator);
                psd::DestroyImageDataSection(dataSection, &allocator);
                file.Close();
                return result;
            }

            if (channel_count > 1)
            {

                psd::AlphaChannel *channel_resource = &resourcesSection->alphaChannels[0];
                for (unsigned int i = 1; i < channel_count; i++)
                {
                    image->channels[i+1] = new Channel();

                    int result = getAlphaChannel(dataSection->images[i+1].data, document, image->channels[i+1], channel_resource, i);
                    // if failed to get channel, destroy document  resources and data section and return
                    if (result < 0)
                    {
                        psd::DestroyDocument(document, &allocator);
                        psd::DestroyImageResourcesSection(resourcesSection, &allocator);
                        psd::DestroyImageDataSection(dataSection, &allocator);
                        file.Close();
                        return result;
                    }
                    channel_resource++;
                }
            }
        }
        case psd::colorMode::DUOTONE:
        {
            // not supported yet
            return DUOTONE_NOT_SUPPORTED;
        }
        break;
        case psd::colorMode::INDEXED:
        {
            // not supported yet
            return INDEXED_NOT_SUPPORTED;
        }
        case psd::colorMode::MULTICHANNEL:
        {
            psd::AlphaChannel *channel_resource = &resourcesSection->alphaChannels[0];
            for (unsigned int i = 0; i < channel_count; i++)
            {
                image->channels[i] = new Channel();
                int result = getAlphaChannel(dataSection->images[i].data, document, image->channels[i], channel_resource, i);
                // if failed to get channel, destroy document  resources and data section and return
                if (result < 0)
                {
                    psd::DestroyDocument(document, &allocator);
                    psd::DestroyImageResourcesSection(resourcesSection, &allocator);
                    psd::DestroyImageDataSection(dataSection, &allocator);
                    file.Close();
                    return result;
                }
                channel_resource++;
            }
        }
        break;
        default:
            return UNKNOWN_COLOR_MODE;
            break;
        }

        // set image properties
        image->width = document->width;
        image->height = document->height;
        image->channelCount = document->channelCount;
        if (document->colorMode != psd::colorMode::MULTICHANNEL && document->channelCount > 4)
            image->colorMode = psd::colorMode::MULTICHANNEL;
        else
            image->colorMode = document->colorMode;

        // destroy document  resources and data section
        psd::DestroyDocument(document, &allocator);
        psd::DestroyImageResourcesSection(resourcesSection, &allocator);
        psd::DestroyImageDataSection(dataSection, &allocator);
        file.Close();

        return 0;
    }
}

IMAGE_NAMESPACE_END
