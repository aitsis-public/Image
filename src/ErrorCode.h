#include <string>

enum DECODE_PSD_ERROR_CODE 
{
    BITMAP_NOT_SUPPORTED = -1001,
    GRAYSCALE_NOT_SUPPORTED = -1002,
    INDEXED_NOT_SUPPORTED = -1003,
    DUOTONE_NOT_SUPPORTED = -1004,
    UNKNOWN_COLOR_MODE = -1000,
    FAILED_OPEN_FILE = -1010,
    FAILED_TO_PARSE_IMAGE = -1011,
    UNKNOWN_BIT_DEPTH = -1020,
};

enum EXPORT_ERROR_CODE
{
    PNG_WRITE_ERROR = -2001,
    JPEG_WRITE_ERROR = -2002,
};


enum IMAGEPROC_ERROR_CODE
{
    UPSCALE_NOT_SUPPORTED = -3001,


};

enum IMAGE_ERROR_CODE
{
    UNSUPPORTED_FORMAT = -4001,
    INTERLEAVE_ERROR = -4002,
    NULL_BUFFER = -4003,

};

inline std::string DecodeErrorCode(int ERR_CODE)
{
    std::string err;
    switch (ERR_CODE)
    {
    case BITMAP_NOT_SUPPORTED:
        err = "Bitmap not supported.";
        break;
    case GRAYSCALE_NOT_SUPPORTED:
        err = "Grayscale not supported.";
        break;
    case INDEXED_NOT_SUPPORTED:
        err = "Indexed not supported.";
        break;
    case DUOTONE_NOT_SUPPORTED:
        err = "Duotone not supported.";
        break;
    case UNKNOWN_COLOR_MODE:
        err = "Unknown color mode.";
        break;
    case FAILED_OPEN_FILE:
        err = "Failed to open file.";
        break;
    case FAILED_TO_PARSE_IMAGE:
        err = "Failed to parse image.";
        break;
    case UNKNOWN_BIT_DEPTH:
        err = "Unknown bit depth.";
        break;
    case PNG_WRITE_ERROR:
        err = "Failed to write PNG file.";
        break;
    case JPEG_WRITE_ERROR:
        err = "Failed to write JPEG file.";
        break;
    case UPSCALE_NOT_SUPPORTED:
        err = "Upscale not supported.";
        break;
    case UNSUPPORTED_FORMAT:
        err = "Unsupported format.";
        break;
    case INTERLEAVE_ERROR:
        err = "Failed to interleave RGB.";
        break;
    case NULL_BUFFER:
        err = "Buffer is null.";
        break;
    
    default:
        err = "Unknown error.";
        break;
    }
    return err;
}