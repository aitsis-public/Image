#pragma once

#include "Psd.h"
#include "PsdDocument.h"
#include "PsdNativeFile.h"
#include "PsdAllocator.h"
#include "PsdAlphaChannel.h"
#include "Image.h"

IMAGE_NAMESPACE_BEGIN
namespace DecodePsd
{
    int getRGBChannel(void *data, psd::Document *doc, Channel *channels, int channelCount);
    int getCMYKChannel(void *data, psd::Document *doc, Channel *channels, int channelCount);
    int getGrayscaleChannel(void *data, psd::Document *doc, Channel *channel, int channelIndex);
    int getLABChannel(void *data, psd::Document *doc, Channel *channel, int channelIndex);
    int getBitmapChannel(void *data, psd::Document *doc, Channel *channel, int channelIndex);
    int getAlphaChannel(void *data, psd::Document *doc, Channel *channel, psd::AlphaChannel *channel_resource, int channelIndex);
    int getChannelData(void *data, Channel *channel);

    int open(const char *filename, Image *image);
    // int createImage(psd::NativeFile *file, psd::Allocator *allocator, psd::Document *doc, Image *image);
}

IMAGE_NAMESPACE_END